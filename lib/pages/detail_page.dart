import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_query/bloc/detail_page_bloc.dart';
import 'package:github_query/bloc/detail_page_state.dart';
import 'package:github_query/widgets/commit_list.dart';

class DetailPage extends StatefulWidget {
  static String routeName = "/detail";

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  DetailPageBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<DetailPageBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Details")),
      body: BlocBuilder(
        bloc: bloc,
        builder: (context, DetailPageState state) {
          if(state.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else if(state.hasError) {
            return _emptyViewError(state.errorMessage);
          } else if(state.commits.length > 0) {
            return CommitListView(state.commits);
          } else {
            return _emptyView();
          }
        }
      ),
    );
  }
  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  Widget _emptyViewError(String errorMessage) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.error, color: Colors.grey, size: 50,),
          SizedBox(height: 10,),
          Text(errorMessage)
        ],
      ),
    );
  }

  Widget _emptyView() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.inbox, color: Colors.grey, size: 50),
          SizedBox(height: 10,),
          Text("This repository has no commits.")
        ],
      ),
    );
  }
}
