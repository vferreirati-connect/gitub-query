import 'package:flutter/material.dart';
import 'package:github_query/bloc/search_page_bloc.dart';
import 'package:github_query/bloc/search_page_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_query/widgets/repo_list.dart';

class SearchPage extends StatefulWidget {
  static String routeName = "/";

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Icon _searchIcon = Icon(Icons.search);
  Widget _appBarTitle = Text("Search Repositories");
  SearchPageBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SearchPageBloc>(context);
  }

  void _onSearchPressed() {
    setState(() {
      if(_searchIcon.icon == Icons.search) {
        _searchIcon = Icon(Icons.close);
        _appBarTitle = TextField(
          decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            hintText: "Search Repos"
          ),
          onChanged: bloc.onUpdateQuery,
        );

      } else {
        _searchIcon = Icon(Icons.search);
        _appBarTitle = Text("Search Repositories");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        leading: GestureDetector(child: _searchIcon, onTap: _onSearchPressed),
        centerTitle: true,
        title: _appBarTitle,
      ),
      body: BlocBuilder(
        bloc: bloc,
        builder: (context, SearchPageState currentState) {
          if(currentState.dataHasLoaded) {
            if(currentState.isLoading) {
              return Center(child: CircularProgressIndicator());
            } else if(currentState.hasError) {
              return _emptyViewError(currentState.errorMessage);

            } else if(currentState.repos.length > 0) {
              return RepoListView(currentState.repos);

            } else {
              return _emptyView();
            }
          } else {
            return Center(child: Text("Make a search!"));
          }
        },
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }

  Widget _emptyViewError(String errorMessage) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.error, color: Colors.grey, size: 50,),
          SizedBox(height: 10,),
          Text(errorMessage)
        ],
      ),
    );
  }

  Widget _emptyView() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.inbox, color: Colors.grey, size: 50),
          SizedBox(height: 10,),
          Text("No repository found.")
        ],
      ),
    );
  }
}
