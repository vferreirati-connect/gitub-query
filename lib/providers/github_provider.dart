import 'package:github_query/models/commit.dart';
import 'package:github_query/models/commit_result.dart';
import 'package:github_query/models/repo_result.dart';
import 'package:github_query/models/repository.dart';
import 'package:github_query/providers/base_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GithubProvider extends BaseProvider {

  Future<RepoResult> searchRepos(String query, {int pageNum = 1}) async {

    var queryUrl = _getSearchUrl(query, pageNum: pageNum);
    var response = await http.get(queryUrl);

    if(response.statusCode == 200) {
      var responseMap = json.decode(response.body);
      var repoList = (responseMap["items"] as List);

      var repoObjects = List<Repository>();
      repoList.forEach((repo) {
        repoObjects.add(Repository.fromJson(repo));
      });

      return RepoResult(repoObjects, _hasNextPage(response.headers));
    } else if(response.statusCode == 403) {
      return RepoResult(null, false, errorMessage: "You're doing this too fast! Try again soon.");
    } else {
      return RepoResult(null, false, errorMessage: "API returned status code ${response.statusCode}");
    }
  }

  Future<CommitResult> getCommits(String userLogin, String repoName, {int pageNum = 1}) async {
    var queryUrl = _getCommitsUrl(userLogin, repoName, pageNum: pageNum);
    var response = await http.get(queryUrl);

    if(response.statusCode == 200) {
      var responseMap = json.decode(response.body);
      var commitList = (responseMap as List);

      var commitObjects = List<Commit>();
      commitList.forEach((commit) {
        commitObjects.add(Commit.fromJson(commit));
      });

      return CommitResult(commitObjects, _hasNextPage(response.headers));
    } else if(response.statusCode == 403) {
      return CommitResult(null, false,
          errorMessage: "You're doing this too fast! Try again soon.");
    } else if(response.statusCode == 409) {
      return CommitResult(List<Commit>(), false);
    } else {
      return CommitResult(null, false, errorMessage: "API returned status code ${response.statusCode}");
    }

  }

  String _getSearchUrl(String query, {int pageNum = 1}) {
    return "$githubBaseUrl/search/repositories?q=$query&sort=stars&order=desc&page=$pageNum";
  }

  String _getCommitsUrl(String userLogin, String repoName, {int pageNum = 1}) {
    return "$githubBaseUrl/repos/$userLogin/$repoName/commits?page=$pageNum";
  }

  bool _hasNextPage(Map<String, String> headers) {
    return headers["link"] != null ? headers["link"].contains("next") : false;
  }
}
