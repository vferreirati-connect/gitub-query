import 'package:github_query/models/repository.dart';

class SearchPageState {
  List<Repository> repos;
  int currentPage;
  bool hasNextPage;
  bool isLoading;
  bool dataHasLoaded;
  String errorMessage;
  bool get hasError => errorMessage != null;

  SearchPageState._({this.repos, this.currentPage, this.hasNextPage, this.isLoading, this.dataHasLoaded, this.errorMessage});

  factory SearchPageState.empty() => SearchPageState._(
    repos: null,
    currentPage: 1,
    hasNextPage: true,
    isLoading: false,
    dataHasLoaded: false
  );
}