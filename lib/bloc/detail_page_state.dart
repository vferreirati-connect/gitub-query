import 'package:github_query/models/commit.dart';

class DetailPageState {
  List<Commit> commits;
  bool isLoading;
  bool hasNextPage;
  int currentPage;
  String errorMessage;
  bool get hasError => errorMessage != null;

  DetailPageState._({this.commits, this.isLoading, this.currentPage, this.hasNextPage, this.errorMessage});

  factory DetailPageState.empty() {
    return DetailPageState._(
      commits: List(),
      isLoading: true,
      hasNextPage: false,
      currentPage: 1,
    );
  }
}