import 'package:bloc/bloc.dart';
import 'package:github_query/bloc/search_page_state.dart';
import 'package:github_query/providers/github_provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:connectivity/connectivity.dart';

abstract class SearchPageEvent {}
class StartRepoQuery extends SearchPageEvent {
  String query;
  StartRepoQuery(this.query);
}
class QueryRepos extends SearchPageEvent {
  String query;
  QueryRepos(this.query);
}

class SearchPageBloc extends Bloc<SearchPageEvent, SearchPageState> {
  GithubProvider _githubProvider;
  BehaviorSubject<String> _queryController = new BehaviorSubject();

  SearchPageBloc(this._githubProvider) {
    _queryController.stream.debounce(Duration(seconds: 1)).listen(_onQuery);
  }

  @override
  get initialState => SearchPageState.empty();

  void onUpdateQuery(String query) {
    if(query != null && query.trim().isNotEmpty) {

      _queryController.add(query);
    }
  }

  void _onQuery(String query) {
    dispatch(StartRepoQuery(query));
    print("Querying $query");
  }

  @override
  Stream<SearchPageState> mapEventToState(currentState, SearchPageEvent event) async* {
    if(event is StartRepoQuery) {
      yield _startQuery(event.query);
    } else if(event is QueryRepos) {
      yield await _searchRepos(event.query);
    }
  }

  SearchPageState _startQuery(String query) {
      dispatch(QueryRepos(query));
      var newState = SearchPageState.empty();
      newState.isLoading = true;
      newState.dataHasLoaded = true;
      return newState;
  }

  Future<SearchPageState> _searchRepos(String query) async {
    var conn = await (new Connectivity().checkConnectivity());
    if(conn != ConnectivityResult.none) {
      var repoResult = await _githubProvider.searchRepos(query);

      var newState = SearchPageState.empty();
      newState.errorMessage = repoResult.errorMessage;
      newState.hasNextPage = repoResult.hasNextPage;
      newState.currentPage = currentState.currentPage + 1;
      newState.repos = repoResult.repositoryList;
      newState.isLoading = false;
      newState.dataHasLoaded = true;

      return newState;
    } else {
      print("Device offline");
      var newState = SearchPageState.empty();
      newState.errorMessage = "Device is offline";
      newState.isLoading = false;
      newState.dataHasLoaded = true;
      return newState;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _queryController.close();
  }
}