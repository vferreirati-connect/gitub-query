import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:github_query/bloc/detail_page_state.dart';
import 'package:github_query/providers/github_provider.dart';

abstract class DetailPageEvent {}
class LoadData extends DetailPageEvent {}

class DetailPageBloc extends Bloc<DetailPageEvent, DetailPageState> {
  String _user;
  String _repo;
  GithubProvider _githubProvider;

  DetailPageBloc(this._repo, this._user, this._githubProvider) {
    init();
  }

  void init() => dispatch(LoadData());

  @override
  DetailPageState get initialState => DetailPageState.empty();

  @override
  Stream<DetailPageState> mapEventToState(DetailPageState currentState, DetailPageEvent event) async* {
    if(event is LoadData) {
      yield await loadData();
    }
  }

  Future<DetailPageState> loadData() async {
    var newState = DetailPageState.empty();
    var con = await (new Connectivity()).checkConnectivity();
    if(con != ConnectivityResult.none) {
      var commitResult = await _githubProvider.getCommits(_user, _repo);
      newState.errorMessage = commitResult.errorMessage;
      newState.commits = commitResult.commits;
      newState.hasNextPage = commitResult.hasNextPage;
      newState.currentPage = currentState.currentPage + 1;
      newState.isLoading = false;
    } else {
      newState.errorMessage = "Device is offline";
      newState.hasNextPage = false;
      newState.isLoading = false;

    }
    return newState;
  }
}