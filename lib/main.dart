import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_query/bloc/detail_page_bloc.dart';
import 'package:github_query/bloc/search_page_bloc.dart';
import 'package:github_query/pages/detail_page.dart';
import 'package:github_query/pages/search_page.dart';
import 'package:github_query/providers/github_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var githubProvider = GithubProvider();
    return MaterialApp(
      theme: ThemeData.dark(),
      title: "Repo Searcher",
      routes: {
        SearchPage.routeName: (context) => BlocProvider(
          child: SearchPage(),
          bloc: SearchPageBloc(githubProvider)
        ),
      },
      onGenerateRoute: (routeSettings) {
        var splitRoute = routeSettings.name.split("/");
        if(splitRoute[0] != "") {
          return null;
        }

        if(splitRoute[1] == "detail") {
          var user = splitRoute[2];
          var repo = splitRoute[3];

          return MaterialPageRoute(builder: (context) => BlocProvider(
            child: DetailPage(),
            bloc: DetailPageBloc(repo, user, githubProvider),
          ));
        }

        return null;
      },
      onUnknownRoute: (routeSettings) {
        print("Tried to access unknown route ${routeSettings.name}");
        return MaterialPageRoute(builder: (context) => BlocProvider(
          child: SearchPage(),
          bloc: SearchPageBloc(githubProvider),
        ));
      },
    );
  }
}
