import 'package:flutter/material.dart';
import 'package:github_query/models/repository.dart';
import 'package:github_query/pages/detail_page.dart';
import 'package:url_launcher/url_launcher.dart';

class RepoCard extends StatelessWidget {
  final Repository repo;

  RepoCard(this.repo);

  void _onInfoTap(BuildContext context) {
    var route = "${DetailPage.routeName}/${repo.owner.userLogin}/${repo.name}";
    print("Accessing route $route");
    Navigator.of(context).pushNamed(route);
  }

  void _onWebTap(BuildContext context) async {
    if(await canLaunch(repo.htmlUrl)) {
      await launch(repo.htmlUrl);
    } else {
      var snackBar = SnackBar(
        content: Text("Failed to open web browser"),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  Widget _starsBadge() {
    return Column(
      children: <Widget>[
        SizedBox(width: 60,),
        Icon(Icons.star, color: Colors.yellow, size: 20,),
        SizedBox(height: 5,),
        Text(repo.starsCount.toString(), style: TextStyle(fontSize: 10),)
      ],
    );
  }

  Widget _forksBadge() {
    return Column(
      children: <Widget>[
        SizedBox(width: 60,),
        Icon(Icons.device_hub, size: 20, color: Colors.grey,),
        SizedBox(height: 5,),
        Text(repo.forksCount.toString(), style: TextStyle(fontSize: 10)),
      ]
    );
  }

  Widget _infoBadge(BuildContext context) {
    return GestureDetector(
      onTap: () => _onInfoTap(context),
      child: Column(
        children: <Widget>[
          SizedBox(width: 60,),
          Icon(Icons.info_outline, color: Colors.blue, size: 20),
          SizedBox(height: 5,),
          Text("Info", style: TextStyle(fontSize: 10))
        ],
      ),
    );
  }

  Widget _webBadge(BuildContext context) {
    return GestureDetector(
      onTap: () => _onWebTap(context),
      child: Column(
        children: <Widget>[
          SizedBox(width: 60,),
          Icon(Icons.language, color: Colors.blue, size: 20),
          SizedBox(height: 5,),
          Text("Page", style: TextStyle(fontSize: 10))
        ],
      ),
    );
  }

  Widget _repoAndOwnerInfo() {
    return ListTile(
      leading: CircleAvatar(
        backgroundImage: NetworkImage(repo.owner.avatarUrl),
        radius: 25
      ),
      title: Text(repo.name),
      subtitle: Text("Owner: ${repo.owner.userLogin}")
    );
  }

  Widget _statsInfo(BuildContext context) {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _starsBadge(),
        _forksBadge(),
        _infoBadge(context),
        _webBadge(context)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        child: Column(
          children: <Widget>[
            _repoAndOwnerInfo(),
            _statsInfo(context),
          ],
        ),
      ),
    );
  }
}
