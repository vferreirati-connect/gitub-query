import 'package:flutter/material.dart';
import 'package:github_query/models/commit.dart';
import 'package:github_query/widgets/commit_card.dart';

class CommitListView extends StatelessWidget {
  final List<Commit> commits;

  CommitListView(this.commits);

  Widget _commitItem(BuildContext context, int index) {
    return CommitCard(commits[index]);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: commits.length,
      itemBuilder: _commitItem
    );
  }
}
