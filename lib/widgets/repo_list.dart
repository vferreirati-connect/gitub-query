import 'package:flutter/material.dart';
import 'package:github_query/models/repository.dart';
import 'package:github_query/widgets/repo_card.dart';

class RepoListView extends StatelessWidget {
  final List<Repository> repos;

  RepoListView(this.repos);

  Widget _buildItem(BuildContext context, int index) {
    return RepoCard(repos[index]);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: repos.length,
      itemBuilder: _buildItem,
    );
  }
}
