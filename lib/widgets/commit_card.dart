import 'package:flutter/material.dart';
import 'package:github_query/models/commit.dart';
import 'package:url_launcher/url_launcher.dart';

class CommitCard extends StatelessWidget {
  final Commit commit;

  CommitCard(this.commit);

  void _onTap(BuildContext context) async {
    if(await canLaunch(commit.htmlUrl)) {
      await launch(commit.htmlUrl);
    } else {
      var snackBar = SnackBar(
        content: Text("Failed to open web browser"),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  Widget _avatarImage() {
    if(commit.committerAvatarUrl == null) {
      return CircleAvatar(
        child: Text(commit.committerName[0]),
        radius: 25,
      );
    } else {
      return CircleAvatar(
        backgroundImage: NetworkImage(commit.committerAvatarUrl),
        radius: 25
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onTap(context),
      child: Card(
        child: Container(
          padding: EdgeInsets.all(5),
          child: ListTile(
            title: Text(commit.message),
            subtitle: Text(commit.committerName),
            leading: _avatarImage(),
          ),
        ),
      ),
    );
  }
}
