import 'package:github_query/models/owner.dart';

class Repository {
  String name;
  String htmlUrl;
  int starsCount;
  int forksCount;
  Owner owner;

  Repository._({this.name, this.forksCount, this.htmlUrl, this.owner, this.starsCount});

  factory Repository.fromJson(Map<String, dynamic> map) {
    return Repository._(
      name: map["name"],
      owner: Owner.fromJson(map["owner"]),
      htmlUrl: map["html_url"],
      starsCount: map["stargazers_count"],
      forksCount: map["forks_count"]
    );
  }
}