class Owner {
  String avatarUrl;
  String userLogin;

  Owner._({this.avatarUrl, this.userLogin});

  factory Owner.fromJson(Map<String, dynamic> map) {
    return Owner._(
      avatarUrl: map["avatar_url"],
      userLogin: map["login"]
    );
  }
}