import 'package:github_query/models/repository.dart';

class RepoResult {
  List<Repository> repositoryList;
  bool hasNextPage;
  String errorMessage;

  bool get hasError => errorMessage != null;

  RepoResult(this.repositoryList, this.hasNextPage, {this.errorMessage});
}