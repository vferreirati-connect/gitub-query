class Commit {
  String htmlUrl;
  String committerName;
  String committerAvatarUrl;
  String message;

  Commit._({this.htmlUrl, this.committerAvatarUrl, this.committerName, this.message});

  factory Commit.fromJson(Map<String, dynamic> map) {
    var avatarUrl = map["committer"] != null
      ? map["committer"]["avatar_url"]
      : null;

    return Commit._(
      htmlUrl: map["html_url"],
      message: map["commit"]["message"],
      committerAvatarUrl: avatarUrl,
      committerName: map["commit"]["committer"]["name"]
    );
  }
}