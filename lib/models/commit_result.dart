import 'package:github_query/models/commit.dart';

class CommitResult {
  List<Commit> commits;
  bool hasNextPage;
  String errorMessage;

  bool get hasError => errorMessage != null;

  CommitResult(this.commits, this.hasNextPage, {this.errorMessage});
}
